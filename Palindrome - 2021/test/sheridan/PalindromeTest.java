package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindromeRegular( ) {
		assertTrue("InValid input",Palindrome.isPalindrome("malayalam"));
	}

	@Test
	public void testIsPalindromeException( ) {
		assertFalse("Exceptional Case", Palindrome.isPalindrome("helloworld"));
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		assertTrue("InValid input",Palindrome.isPalindrome("Tacocat "));
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		assertFalse("Exceptional Case", Palindrome.isPalindrome("taco's cat"));
		
	}	
	
}
